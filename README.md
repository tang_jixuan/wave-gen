# waveGen

#### 介绍
为ST3020测试提供生成可视化波形的程序

#### 软件架构
软件架构说明
test.sh

#### 安装教程

1.  iverilog
2.  python3
3.  .VCD Wave Viewer(例如GTKWave)

#### 使用说明

1.  将波形按格式写入test.wave
2.  在waveGen.py中调整clockPeriod等参数
3.  运行test.sh

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request

#### 贡献者
1. 唐集轩
2. 三极管(Triode)
