# Parameters
waveSourceFilename = "test.wave"
verilogFilename = "test.v"
moduleName = "waveGen"
clockPeriod = 10  # ns
transitionPoint = 2  # ns
waveWidth = 6  # ns
#! Significant Codes DO NOT EDIT !!!
signalNames = []
waveMatrix = []
signalNum = 0
waveLines = 0
signals = {"NRZ0": [], "NRZ": [], "RZ": [], "RO": []}
with open(waveSourceFilename, "r") as waveSource:
    signalNames = waveSource.readline().split()
    formats = waveSource.readline().split()
    waveMatrix = waveSource.readlines()
    signalNum = len(signalNames)
    waveLines = len(waveMatrix)
    for i in range(signalNum):
        signals[formats[i]].append(signalNames[i])
targetLines = ["`timescale  1ns / 1ps\n", "module waveGen;\n"]
for signalName in signalNames:
    targetLines.append("reg {};\n".format(signalName))
targetLines.extend(["initial\n",
                    "begin\n",
                    '$dumpfile("wave.vcd");\n',
                    '$dumpvars;\n',
                    'end\n'])
targetLines.append("initial\n")
targetLines.append("begin\n")
for signalName in signalNames:
    targetLines.append("{} = 1'b0;\n".format(signalName))
targetLines.append("#{};\n".format(clockPeriod))
for signalLine in waveMatrix:
    if(signalLine == "\n"):
        continue
    wave = {}
    for i in range(signalNum):
        wave[signalNames[i]] = signalLine[i]
    for signalName in signals["RZ"]:
        targetLines.append("{} = 1'b0;\n".format(signalName))
    for signalName in signals["RO"]:
        targetLines.append("{} = 1'b1;\n".format(signalName))
    for signalName in signals["NRZ0"]:
        targetLines.append("{} = 1'b{};\n".format(signalName, wave[signalName]))
    targetLines.append("#{};\n".format(transitionPoint))
    for signalName in signals["RZ"]:
        if(int(wave[signalName])):
            targetLines.append("{} = 1'b{};\n".format(
                signalName, wave[signalName]))
    for signalName in signals["RO"]:
        if(not int(wave[signalName])):
            targetLines.append("{} = 1'b{};\n".format(
                signalName, wave[signalName]))
    for signalName in signals["NRZ"]:
        targetLines.append("{} = 1'b{};\n".format(signalName, wave[signalName]))
    targetLines.append("#{};\n".format(waveWidth))
    for signalName in signals["RZ"]:
        if(int(wave[signalName])):
            targetLines.append("{} = 1'b0;\n".format(
                signalName))
    for signalName in signals["RO"]:
        if(not int(wave[signalName])):
            targetLines.append("{} = 1'b1;\n".format(
                signalName))
    targetLines.append("#{};\n".format(clockPeriod-waveWidth-transitionPoint))
targetLines.append("$finish;\n")
targetLines.append("end\n")
targetLines.append("endmodule\n")
with open(verilogFilename, "w", encoding="utf-8") as verilogTarget:
    verilogTarget.writelines(targetLines)
